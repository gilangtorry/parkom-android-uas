package dev.gilang.parkom.api;

import java.util.List;

import dev.gilang.parkom.models.ParkirModel;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

public interface NetworkService {
    @POST("getData")
    Observable<ApiResponse<List<ParkirModel>>> getData(@Body ParkirModel model);

    @POST("masuk")
    Observable<ApiResponse> masuk(@Body ParkirModel model);

    @POST("keluar")
    Observable<ApiResponse> keluar(@Body ParkirModel model);

    @GET("no_parkir")
    Observable<ApiResponse<List<ParkirModel>>> getNomor();

}
