package dev.gilang.parkom.api;

import java.util.List;

import dev.gilang.parkom.models.ParkirModel;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ApiModule {
    private static ApiModule mInstance;
    private NetworkService service;

    public static ApiModule getInstance() {
        if (mInstance == null) mInstance = new ApiModule(ApiConfig.getRetrofit().create(NetworkService.class));
        return mInstance;
    }

    private ApiModule(NetworkService service) {
        super();
        this.service = service;
    }

    public Observable<ApiResponse<List<ParkirModel>>> getNomor()
    {
        return service.getNomor()
                .map(response -> response)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ApiResponse> masuk(ParkirModel param)
    {
        return service.masuk(param)
                .map(response -> response)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ApiResponse> keluar(ParkirModel param)
    {
        return service.keluar(param)
                .map(response -> response)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ApiResponse<List<ParkirModel>>> listParkir(ParkirModel param)
    {
        return service.getData(param)
                .map(response -> response)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
