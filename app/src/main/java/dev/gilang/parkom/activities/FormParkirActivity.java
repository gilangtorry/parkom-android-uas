package dev.gilang.parkom.activities;

import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dev.gilang.parkom.models.ParkirModel;
import dev.gilang.parkom.R;
import dev.gilang.parkom.api.ApiConfig;
import dev.gilang.parkom.api.ApiModule;
import dev.gilang.parkom.api.NetworkService;

public class FormParkirActivity extends BaseActivity implements TextToSpeech.OnInitListener {
    private static final String TAG = FormParkirActivity.class.getSimpleName();
    @BindView(R.id.tvTitle) TextView tvTitle;
    @BindView(R.id.ic_back) ImageView ic_back;
    @BindView(R.id.tvNomor) TextView tvNomor;
    @BindView(R.id.edNopol) EditText edNopol;
    @BindView(R.id.rgJenis) RadioGroup rgJenis;
    @BindView(R.id.rgPengendara) RadioGroup rgPengendara;
    @BindView(R.id.rbMotor) RadioButton rbMotor;
    @BindView(R.id.rbMobil) RadioButton rbMobil;
    @BindView(R.id.rbMahsiswa) RadioButton rbMahsiswa;
    @BindView(R.id.rbStaff) RadioButton rbStaff;
    @BindView(R.id.rbTamu) RadioButton rbTamu;

    private TextToSpeech mTts;
    private NetworkService networkService;
    private String jenis;
    private String pengendara;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_parkir);
        ButterKnife.bind(this);
        networkService = ApiConfig.getRetrofit().create(NetworkService.class);
        tvTitle.setText("Parkir Masuk");
        ic_back.setVisibility(View.VISIBLE);
        ic_back.setOnClickListener(v -> finish());
        mTts = new TextToSpeech(this, this);
        mTts.setSpeechRate(0.8f);

        //default
        jenis = rbMotor.getText().toString();
        rgJenis.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId){
                case R.id.rbMotor:
                    jenis = rbMotor.getText().toString();
                    break;
                case R.id.rbMobil:
                    jenis = rbMobil.getText().toString();
                    break;
            }
        });
        //default
        pengendara = rbMahsiswa.getText().toString();
        rgPengendara.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId){
                case R.id.rbMahsiswa:
                    pengendara = rbMahsiswa.getText().toString();
                    break;
                case R.id.rbStaff:
                    pengendara = rbStaff.getText().toString();
                    break;
                case R.id.rbTamu:
                    pengendara = rbTamu.getText().toString();
                    break;
            }
        });

        getNomor();
    }

    @OnClick(R.id.btnBack)
    public void back(){
       finish();
    }

    @OnClick(R.id.btnMasuk)
    public void masuk(){
        if(!edNopol.getText().toString().trim().equalsIgnoreCase(""))
            sendData();
        else message("Nopol belum di isi");


    }

    private void getNomor(){
        showDialogs();
        compositeSubscription.add(
                ApiModule.getInstance()
                        .getNomor()
                        .subscribe(d -> {
                           hideDialog();
                           if(d.status == 200 && d.getResponse().size() > 0){
                               int nP;
                               if(d.getResponse().get(0).no_parkir != null)
                                   nP = Integer.parseInt(d.getResponse().get(0).no_parkir) + 1;
                               else nP = 1;
                               tvNomor.setText(""+nP);
                           }else message(d.message);
                        }, t -> {
                            hideDialog();
                            message(t.getMessage());
                        })
        );
    }

    private void sendData(){
        showDialogs();
        ParkirModel param = new ParkirModel();
        param.no_parkir = tvNomor.getText().toString().trim();
        param.nopol = edNopol.getText().toString().trim();
        param.jenis = jenis;
        param.pengendara = pengendara;
        compositeSubscription.add(
                ApiModule.getInstance()
                        .masuk(param)
                        .subscribe(d -> {
                            hideDialog();
                            message(d.message);
                            if(d.status == 200){
                                mTts.speak("silahkan masuk",
                                        TextToSpeech.QUEUE_FLUSH,
                                        null);
                                new Handler().postDelayed(() -> finish(),2000);
                            }
                        }, t -> {
                            hideDialog();
                            message(t.getMessage());
                        })
        );

    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = mTts.setLanguage(new Locale("id", "ID"));
            if (result == TextToSpeech.LANG_MISSING_DATA ||
                    result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.v(TAG, "Language is not available.");
            }
        } else {
            Log.v(TAG, "Could not initialize TextToSpeech.");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTts != null) {
            mTts.stop();
            mTts.shutdown();
        }
    }
}
