package dev.gilang.parkom.activities;

import android.content.Intent;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;
import dev.gilang.parkom.R;
import dev.gilang.parkom.activities.BaseActivity;
import dev.gilang.parkom.activities.FormParkirActivity;
import dev.gilang.parkom.activities.ListParkirActivity;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.imgMasuk)
    public void masuk(){
        startActivity(new Intent(this, FormParkirActivity.class));
    }

    @OnClick(R.id.imgKeluar)
    public void keluar(){
        startActivity(new Intent(this, ListParkirActivity.class));
    }


}
