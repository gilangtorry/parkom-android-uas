package dev.gilang.parkom.activities;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import dev.gilang.parkom.adapters.ParkirAdapter;
import dev.gilang.parkom.models.ParkirModel;
import dev.gilang.parkom.R;
import dev.gilang.parkom.api.ApiModule;

public class ListParkirActivity extends BaseActivity implements TextToSpeech.OnInitListener{
    private static final String TAG = ListParkirActivity.class.getSimpleName();
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ic_back)
    ImageView ic_back;
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.edNopol)
    EditText edNopol;

    ParkirAdapter adapter;
    private TextToSpeech mTts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_parkir);
        ButterKnife.bind(this);
        tvTitle.setText("Parkir Keluar");
        ic_back.setVisibility(View.VISIBLE);
        ic_back.setOnClickListener(v -> finish());

        adapter = new ParkirAdapter(this);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(adapter);
        adapter.setOnItemClickListener((view, obj, position) -> {
            showData(obj);
        });

        mTts = new TextToSpeech(this, this);
        mTts.setSpeechRate(0.8f);
        getData("");

        edNopol.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()>1){
                    getData(s.toString());
                }else getData("");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void getData(String no_pol){
        //showDialogs();
        ParkirModel param = new ParkirModel();
        param.nopol = no_pol;
        compositeSubscription.add(
                ApiModule.getInstance()
                        .listParkir(param)
                        .subscribe(d -> {
                            hideDialog();
                            if(d.status == 200){
                                if(d.getResponse().size() > 0){
                                    adapter.setData(d.getResponse());
                                }else message("Parkir kosong");
                            }else adapter.clearData();
                        }, t -> {
                            hideDialog();
                            message(t.getMessage());
                        })
        );
    }

    private void doKeluar(String id, Dialog dialog){
        showDialogs();
        ParkirModel param = new ParkirModel();
        param.id = id;
        compositeSubscription.add(
                ApiModule.getInstance()
                        .keluar(param)
                        .subscribe(d -> {
                            hideDialog();
                            if(d.status == 200){
                                dialog.dismiss();
                                mTts.speak("Terimakasih",
                                        TextToSpeech.QUEUE_FLUSH,
                                        null);
                                new Handler().postDelayed(() -> finish(),2000);
                            }else message(d.message);
                        }, t -> {
                            hideDialog();
                            message(t.getMessage());
                        })
        );
    }

    Dialog dialog;
    private void showData(ParkirModel data){
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_keluar, null);
        final Button batal = dialogView.findViewById(R.id.btnBack);
        final Button keluar = dialogView.findViewById(R.id.btnKeluar);
        ((TextView)dialogView.findViewById(R.id.tvTnopol)).setText(data.no_parkir);
        ((TextView)dialogView.findViewById(R.id.tvNopol)).setText(String.format(Locale.US,"Nomor Polisi : %s", data.nopol));
        ((TextView)dialogView.findViewById(R.id.tvUrut)).setText(String.format(Locale.US,"Nomor Parkir : %s", data.no_parkir));
        ((TextView)dialogView.findViewById(R.id.tvKend)).setText(String.format(Locale.US,"Kendaraan : %s", data.jenis));
        ((TextView)dialogView.findViewById(R.id.tvPenendara)).setText(String.format(Locale.US,"Pengendara : %s", data.pengendara));
        ((TextView)dialogView.findViewById(R.id.tvMasuk)).setText(String.format(Locale.US,"Parkir Masuk : %s", data.tgl_parkir));
        batal.setOnClickListener(v -> dialog.dismiss());
        keluar.setOnClickListener(v -> {
           doKeluar(data.id, dialog);
        });

        dialogBuilder.setView(dialogView);
        dialog = dialogBuilder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = mTts.setLanguage(new Locale("id", "ID"));
            if (result == TextToSpeech.LANG_MISSING_DATA ||
                    result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.v(TAG, "Language is not available.");
            }
        } else {
            Log.v(TAG, "Could not initialize TextToSpeech.");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTts != null) {
            mTts.stop();
            mTts.shutdown();
        }
    }
}
